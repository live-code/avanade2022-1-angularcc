export interface Device {
  id: number;
  title: string;
  memory: number;
  os: 'android' | 'ios';
  cost: number;
}
