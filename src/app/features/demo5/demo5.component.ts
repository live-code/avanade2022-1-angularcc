import { Component } from '@angular/core';
import { DeviceService } from './services/device.service';
import { NgForm, NgModel } from '@angular/forms';

@Component({
  selector: 'app-demo5',
  template: `
    
    <!--Device form: edit / add-->
    <div class="mymodal" [ngClass]="{open: srv.isModalOpened}">
      <div *ngIf="f.invalid && f.dirty" class="alert alert-danger">form is invalid</div>

      <form #f="ngForm" (submit)=" saveHandler(f)">
        
        <div *ngIf="inputTitle.errors"> 
          <div *ngIf="inputTitle.errors['required']">Campo obbligatorio</div>
          <div *ngIf="inputTitle.errors['minlength']">
            Ti mancano {{inputTitle.errors['minlength']['requiredLength'] - inputTitle.errors['minlength']['actualLength']}} caratteri
          </div>
        </div>
        
        <!--TODO: add validation msg-->
        <input 
          type="text" name="title" 
          class="form-control"
          [ngClass]="{'is-invalid': inputTitle.invalid && inputTitle.dirty, 'is-valid': inputTitle.valid }"
          [ngModel]=" srv.currentDevice?.title" placeholder="title*" required minlength="5"
          #inputTitle="ngModel"
        >
        
        <div class="myprogress" 
             *ngIf="inputTitle.errors"
             [style.width.%]="calcPerc(inputTitle)"></div>
  
        <!--TODO: add validation msg-->
        <input
          class="form-control"
          [ngClass]="{'is-invalid': inputCost.invalid && inputCost.dirty, 'is-valid': inputCost.valid }"
          type="text" name="cost" 
          [ngModel]="srv.currentDevice?.cost" placeholder="cost" required minlength="3"
          #inputCost="ngModel" 
        >

        <div class="myprogress"
             *ngIf="inputCost.errors"
             [style.width.%]="calcPerc(inputCost)"></div>
        
        
        <select
          class="form-control"
          [ngClass]="{'is-invalid': inputOs.invalid && inputOs.dirty, 'is-valid': inputOs.valid }"
          name="os" [ngModel]="srv.currentDevice?.os"  required
          #inputOs="ngModel"
        >
          <option [ngValue]="null">Select OS</option>
          <option value="android">Android</option>
          <option value="ios">IOS</option>    
        </select>
        <button type="submit" [disabled]="f.invalid">SAVE</button>
        <button type="button" (click)="resetHandler(f)">reset</button>
        <button type="button" (click)="srv.closeModal()">close</button>
      </form>
    </div>
    <!--Error-->
    <div class="alert alert-danger" *ngIf=" srv.error">Ahia!</div>
    <hr>
    
    <!--Device List-->
    <i class="fa fa-plus-circle" (click)="srv.isModalOpened = true">add</i>
    <li 
      *ngFor="let device of srv.devices" (click)="srv.selectDevice(device)"
      class="list-group-item"
      [ngClass]="{active: device.id === srv.currentDevice?.id}"
    >
      {{device.id}} - 
      {{device.title}}
      
      <i class="fa fa-trash" (click)="srv.deleteDevice(device, $event)"></i>
      <i class="fa fa-external-link ms-2" [routerLink]="'/devices/' + device.id"></i>
    </li>
  `,
  styles: [`
    .myprogress {
      width: 100%;
      height: 10px;
      background-color: orange;
      margin: 10px 0px;
      border-radius: 10px;
    }
    .mymodal {
      position: fixed;
      top: 0;
      bottom: 0;
      right: 0;
      left: -300px;
      z-index: 10;
      background-color: #ccc;
      max-width: 300px;
      transition: left 1s cubic-bezier(0.83, 0, 0.17, 1);
    }
    .open {
      left: 0;
    }
  `]
})
export class Demo5Component  {

  constructor(public srv: DeviceService) {
  }


  calcPerc(input: NgModel): number {
    if (input.errors && input.errors['minlength']) {
      return (input.errors['minlength']['actualLength'] / input.errors['minlength']['requiredLength']) * 100
    } else {
      return 0
    }
  }

  resetHandler(f: NgForm) {
    f.reset();
    this.srv.reset();
  }

  saveHandler(f: NgForm) {
    this.srv.save(f.value)
      .then(action => {
        if (action === 'edit') {
          f.reset();
        }
      })
  }
}
