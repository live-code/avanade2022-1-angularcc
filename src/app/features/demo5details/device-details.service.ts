import { Injectable } from '@angular/core';
import { Device } from '../../model/device';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DeviceDetailsService {
  data: Device | null = null;

  constructor(private http: HttpClient) {}

  getDevice(deviceId: number) {
    this.http.get<Device>(`http://localhost:3000/devices/${deviceId}`)
      .subscribe(res => {
        this.data = res;
      })
  }

  destroy() {
    console.log('service on destroy')
  }
}
