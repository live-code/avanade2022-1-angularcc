import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Device } from '../model/device';

interface Data {
  id: number;
  cost: number
}


@Component({
  selector: 'app-demo3',
  template: `
    <input type="text" (keydown.enter)="addDevice()" 
           placeholder="title" #titleInput>
    <input type="text" (keydown.enter)="addDevice()" 
           placeholder="cost" #costInput>
    
    <li *ngFor="let device of devices" class="list-group-item">
      <div class="d-flex justify-content-between align-items-center">
        <div>
          <i class="fa fa-android"></i>
          {{device.title}}
        </div>
        <div [style.color]="device.cost >= 1000 ? 'red' : null">
          € {{device.cost}}
          <i class="fa fa-trash-o" (click)="deleteDevice(device.id, $event)"></i> 
        </div>
      </div>
    </li>
  `,
})
export class Demo3Component {
  @ViewChild('titleInput') inputTitle!: ElementRef<HTMLInputElement>;
  @ViewChild('costInput') inputCost!: ElementRef<HTMLInputElement>;
  devices: Device[] = [
    {
      id: 11,
      title: 'IPhone 10',
      cost: 1000,
      memory: 4,
      os: 'ios'
    },
    {
      id: 22,
      title: 'IPhone 13 Pro Max Super Mega',
      cost: 2000,
      memory: 6,
      os: 'ios'
    },
    {
      id: 43,
      title: 'Samsung S21 ',
      cost: 500,
      memory: 16,
      os: 'android'
    }
  ];

  ngAfterViewInit() {
    this.inputTitle.nativeElement.focus()
  }

  addDevice() {
    const title = this.inputTitle.nativeElement;
    const cost = this.inputCost.nativeElement;

    if (title.value.length > 3 && cost.value !== '') {
      this.devices.push({
        id: 55,
        title: title.value,
        cost: +this.inputCost.nativeElement.value,
        memory: 4,
        os: 'ios'
      });
      title.value = '';
      this.inputCost.nativeElement.value = '';
      title.focus();
    }
  }
  
  deleteDevice(id: number, event: MouseEvent) {
    const index = this.devices.findIndex(device => device.id === id)
    this.devices.splice(index, 1)
    // this.devices = this.devices.filter(device => device.id !== id)
  }

  render() {
    console.log('render')
  }

}
