import { Injectable } from '@angular/core';
import { Device } from '../../../model/device';
import { HttpClient } from '@angular/common/http';
import { share, shareReplay } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DeviceService {
  devices: Device[] = []
  currentDevice: Device | null = null;
  error: boolean = false;
  isModalOpened = false;

  constructor(private http: HttpClient) {
    http.get<Device[]>('http://localhost:3000/devices')
      .subscribe((res) => {
        this.devices = res;
      })
  }

  save(device: Device): Promise<'add' | 'edit'> {
    if (this.currentDevice?.id) {
      return this.edit(device);
    } else {
      return this.add(device);
    }
  }

  add(device: Device): Promise<'add' | 'edit'> {
    this.error = false;

    return new Promise((resolve, reject) => {
      this.http.post<Device>('http://localhost:3000/devices', device)
        .subscribe(
          res => {
            this.devices.push(res);
            this.isModalOpened = false;
            resolve('add');
          },
          err => {
            this.error = true;
          }
        )
    })

  }

  edit(device: Device): Promise<'add' | 'edit'> {
    return new Promise((resolve, reject) => {
      this.http.patch<Device>('http://localhost:3000/devices/' + this.currentDevice?.id, device)
        .subscribe(res => {
          const index = this.devices.findIndex(d => d.id === this.currentDevice?.id)
          this.devices[index] = res;
          this.currentDevice = null;
          this.isModalOpened = false;
          resolve('edit')
        })
    })
  }

  selectDevice(device: Device) {
    this.isModalOpened = true;
    this.currentDevice = device;
  }

  reset() {
    this.currentDevice = null;
  }

  deleteDevice(device: Device, event: MouseEvent) {
    event.stopPropagation();
    this.http.delete(`http://localhost:3000/devices/${device.id}`)
      .subscribe(() => {
        const index = this.devices.findIndex(d => d.id === device.id)
        this.devices.splice(index, 1)
        this.currentDevice = null;
      })
  }

  closeModal() {
    this.isModalOpened = false;
    setTimeout(() => {
      this.currentDevice = null
    }, 1000)
  }

}
