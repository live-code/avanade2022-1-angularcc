export type Gender = 'M' | 'F' | 'X';

export interface User {
  id: number;
  name: string;
  age: number;
  gender: Gender;
  city: string;
  birthday: number;
  bitcoins: number;
  address: Address
}

export interface Address {
  city: string;
  street: string
}
