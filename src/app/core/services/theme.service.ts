import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

export type Theme = 'dark' | 'light'

@Injectable({ providedIn: 'root'})
export class ThemeService {
  private value: Theme = 'dark';
  // private value$: Observable<Theme>

  constructor() {
    const theme: Theme = localStorage.getItem('theme') as Theme;
    if (theme) {
      this.value = theme;
    }
  }

  set theme(val: Theme) {
    localStorage.setItem('theme', val)
    this.value = val;
  }

  get theme(): Theme {
    // return localStorage.getItem('theme') as Theme
    return this.value;
  }

}
