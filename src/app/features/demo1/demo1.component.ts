import { Component } from '@angular/core';
import { Gender, User } from '../../model/user'

@Component({
  selector: 'app-demo1',
  template: `
    
     <div>{{user.name}}</div>
     <div [style.font-size.px]="user.age">{{user.age}}</div>
  
     <div
       class="alert"
       [ngClass]="{
          'maleCls': user.gender === 'M', 
          'femaleCls': user.gender === 'F'
       }"
     >
       {{user.gender === 'M' ? 'male' : 'female'}}
       {{getGenderCls(user.gender)}}
     </div>
     
     <img width="100%" [src]="'https://www.mapquestapi.com/staticmap/v5/map?key=Go3ZWai1i4nd2o7kBuAUs4y7pnpjXdZn&center=' + user.city + ' &size=1200,400'" alt="">
     <div>{{user.birthday | date: 'dd MMMM yyyy @ hh:mm'}}</div>
     <div>
       {{user.bitcoins | number: '1.2-4'}}
       
       <span *ngIf="user.bitcoins > 3; ">Sei ricco!</span>
       
       <ng-template #nobitcoins>
         <span>Non hai tanti bitcoin</span>
       </ng-template>

     </div>
     <!--<pre>{{user | json}}</pre>-->
     
     <button (click)="updateCity('Palermo')">Updaate 1</button>
     <button (click)="updateCity('Cagliari')">Update 2</button>
  `,
  styles: [`
    .maleCls {background-color: blue;}
    .femaleCls {background-color: pink;}
    .alert {border: 2px solid black;padding: 10px;}
  `]
})
export class Demo1Component {
  user: User = {
    id: 1,
    name: 'Fabio',
    age: 40,
    gender: 'F',
    city: 'Trieste',
    birthday: 1644231512000,
    address: {
      street: 'via ciccio 123',
      city: 'Trieste',
    },
    bitcoins: 1.4202440298,
  }

  getGenderCls(gender: Gender) {
    switch (gender) {
      case 'F': return 'femaleCls';
      case 'M': return 'maleCls';
      default: return 'xxxx'
    }
  }

  updateCity(city: string) {
    this.user.city = city;
    this.user.gender = 'M'
    this.user.age = 20;
  }
}

// document.getElementById('btn').style.fontSize = '20px'
/*
document.getElementById('btn').addEventListener('click', function() {
  // ...
})

document.getElementById('btn').addEventListener('click', increment)
*/
