import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Device } from '../model/device';

@Component({
  selector: 'app-demo4',
  template: `
    <div *ngIf="f.invalid && f.dirty" class="alert alert-danger">form is invalid</div>
    
    <form #f="ngForm" (submit)="save(f)">
      
      <div *ngIf="inputTitle.errors"> 
        <div *ngIf="inputTitle.errors['required']">Campo obbligatorio</div>
        <div *ngIf="inputTitle.errors['minlength']">
          Ti mancano {{inputTitle.errors['minlength']['requiredLength'] - inputTitle.errors['minlength']['actualLength']}} caratteri
        </div>
      </div>
      
      <!--TODO: add validation msg-->
      <input 
        type="text" name="title" 
        class="form-control"
        [ngClass]="{'is-invalid': inputTitle.invalid && inputTitle.dirty, 'is-valid': inputTitle.valid }"
        [ngModel]="currentDevice?.title" placeholder="title*" required minlength="3"
        #inputTitle="ngModel"
      >

      <!--TODO: add validation msg-->
      <input
        class="form-control"
        [ngClass]="{'is-invalid': inputCost.invalid && inputCost.dirty, 'is-valid': inputCost.valid }"
        type="text" name="cost" 
        [ngModel]="currentDevice?.cost" placeholder="cost" required
        #inputCost="ngModel"
      > 
      
      <select
        class="form-control"
        [ngClass]="{'is-invalid': inputOs.invalid && inputOs.dirty, 'is-valid': inputOs.valid }"
        name="os" [ngModel]="currentDevice?.os"  required
        #inputOs="ngModel"
      >
        <option [ngValue]="null">Select OS</option>
        <option value="android">Android</option>
        <option value="ios">IOS</option>    
      </select>
      <button type="submit" [disabled]="f.invalid">SAVE</button>
      <button type="button" (click)="reset(f)">reset</button>
    </form>
    
    <hr>
    <li 
      *ngFor="let device of devices" (click)="selectDevice(device)"
      class="list-group-item"
      [ngClass]="{active: device.id === currentDevice?.id}"
    >
      {{device.id}} - 
      {{device.title}}
      
      <i class="fa fa-trash" (click)="deleteDevice(device)"></i>
    </li>

    <div>currentDevice: {{currentDevice | json}}</div>

  `,
})
export class Demo4Component  {
  devices: Device[] = [
    {
      id: 11,
      title: 'IPhone 10',
      cost: 1000,
      memory: 4,
      os: 'ios'
    },
    {
      id: 22,
      title: 'IPhone 13 Pro Max Super Mega',
      cost: 2000,
      memory: 6,
      os: 'ios'
    },
    {
      id: 43,
      title: 'Samsung S21 ',
      cost: 500,
      memory: 16,
      os: 'android'
    }
  ];
  currentDevice: Device | null = null;

  save(form: NgForm) {
    if (this.currentDevice?.id) {
      this.edit(form);
    } else {
      this.add(form);
    }
  }

  edit(form: NgForm) {
    const index = this.devices.findIndex(d => d.id === this.currentDevice?.id )
    const active = { ...this.currentDevice, ...form.value } // SPREAD
    this.devices[index] = active;
    this.currentDevice = active;
  }

  add(form: NgForm) {
    this.devices.push({ ...form.value, id: Date.now() })
  }

  selectDevice(device: Device) {
    this.currentDevice = device;
  }

  reset(f: NgForm) {
    f.reset();
    this.currentDevice = null;
  }

  deleteDevice(device: Device) {
    const index = this.devices.findIndex(d => d.id === device.id)
    this.devices.splice(index, 1)
    this.currentDevice = null;
  }
}
