import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HelloComponent } from './features/hello.component';
import { Demo1Component } from './features/demo1/demo1.component';
import { Demo2Component } from './features/demo2.component';
import { Demo3Component } from './features/demo3.component';
import { Demo4Component } from './features/demo4.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Demo5Component } from './features/demo5/demo5.component';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { Demo5detailsComponent } from './features/demo5details/demo5details.component';
import { SettingsComponent } from './features/settings.component';
import { DeviceDetailsService } from './features/demo5details/device-details.service';

@NgModule({
  declarations: [
    AppComponent,
    HelloComponent,
    Demo1Component,
    Demo2Component,
    Demo3Component,
    Demo4Component,
    Demo5Component,
    Demo5detailsComponent,
    SettingsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      { path: 'home', component: HelloComponent },
      { path: 'demo1', component: Demo1Component },
      { path: 'demo2', component: Demo2Component },
      { path: 'demo3', component: Demo3Component },
      { path: 'demo4', component: Demo4Component },
      { path: 'devices', component: Demo5Component },
      { path: 'settings', component: SettingsComponent },
      { path: 'devices/:deviceId', component: Demo5detailsComponent },
      /*{ path: '**', redirectTo: 'home' },*/
      { path: '', redirectTo: 'home', pathMatch: 'full' },
    ])
  ],
  providers: [
   
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
