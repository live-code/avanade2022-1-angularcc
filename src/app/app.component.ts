import { Component } from '@angular/core';
import { ThemeService } from './core/services/theme.service';
import { NavigationEnd, Router } from '@angular/router';
import { distinctUntilChanged, filter, map, switchMap } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  template: `
    <nav 
      class="navbar navbar-expand"
      [ngClass]="{
        'navbar-light bg-light': themeService.theme === 'light',
        'navbar-dark bg-dark': themeService.theme === 'dark'
      }"
    >
      <div class="container-fluid">
        <a class="navbar-brand">
          Navbar
          {{themeService.theme}}

        </a>
       
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" routerLinkActive="bg-dark text-white" routerLink="demo1">1</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" routerLinkActive="bg-dark text-white" routerLink="demo2">2</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" routerLinkActive="bg-dark text-white" routerLink="demo3">3</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" routerLinkActive="bg-dark text-white" routerLink="demo4">4</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" routerLinkActive="bg-dark text-white" routerLink="devices">5</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" routerLinkActive="bg-dark text-white" routerLink="settings">settings</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <hr>
    
    
    <router-outlet></router-outlet>
  `,
})
export class AppComponent {
  constructor(
    public themeService: ThemeService,
    public router: Router,
    public http: HttpClient
  ) {
    router.events
      .pipe(
        filter(ev =>  ev instanceof NavigationEnd),
        map(ev => (ev as NavigationEnd).url),
        // switchMap(url => http.post('http://localhost:3000/logs', { url })),
      )
      .subscribe(res => {
        console.log('success', res)
      })
  }
}
