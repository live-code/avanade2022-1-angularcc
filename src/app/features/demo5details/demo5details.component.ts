import { Component } from '@angular/core';
import { DeviceDetailsService } from './device-details.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-demo5details',
  template: `
   <div *ngIf="!srv.data">loading...</div>
   
   <div *ngIf="srv.data">
     <h1 class="alert alert-success">{{srv.data?.title}}</h1>
     <p>€ {{srv.data.cost}}</p>
   </div>
  `,
})
export class Demo5detailsComponent {
  constructor(
    public srv: DeviceDetailsService,
    private activatedRoute: ActivatedRoute
  ) {
    const deviceId = this.activatedRoute.snapshot.params['deviceId'];
    srv.getDevice(deviceId)
  }

  ngOnDestroy() {
    this.srv.destroy();
  }
}
