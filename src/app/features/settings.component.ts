import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Theme, ThemeService } from '../core/services/theme.service';
import { debounceTime, distinctUntilChanged, filter, fromEvent, map, Observable, switchMap } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { FormControl } from '@angular/forms';
import { Device } from '../model/device';

@Component({
  selector: 'app-settings',
  template: `
    <p>
      Current theme is: {{themeService.theme}}
    </p>
    
    <input type="text" placeholder="search something" [formControl]="input" >
    
    <button (click)="themeService.theme = 'dark'">Set Dark</button>
    <button (click)="themeService.theme = 'light'">Set Light</button>

    <hr>
    <li *ngFor="let d of (devices$ | async)">
      {{d.title}}
    </li>
  `,
})
export class SettingsComponent {
  input = new FormControl();
  devices$ = this.http.get<Device[]>('http://localhost:3000/devices')

  constructor(public themeService: ThemeService, private http: HttpClient) {

    this.input.valueChanges
      .pipe(
        filter(text => text.length > 2),
        debounceTime(1000),
        distinctUntilChanged(),
        switchMap(text => this.http.get(`https://api.openweathermap.org/data/2.5/weather?q=${text}&units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`))
      )
      .subscribe(meteo => {
        console.log('http', meteo)
      })
  }

  ngAfterViewInit() {

  }

  timer: number = 0;

  search() {
    // debounce
    /*clearTimeout(this.timer)
    this.timer = window.setTimeout(() => {
      console.log('http', input.value)
    }, 1000)*/
  }
}
